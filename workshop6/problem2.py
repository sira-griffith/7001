while(True):
    # GET INPUT AND SPLITE IN TO LIST
    list1 = input("List 1: ").split()
    list2 = input("List 2: ").split()
    
    # CONVERT INTO INTEGER TYPE
    list1 = [int(num) for num in list1]
    list2 = [int(num) for num in list2]
    
    # CALCULATE SUM FROM FIRST AND LAST INDEX
    sum1 = list1[0] + list1[-1] if len(list1) > 1 else list1[0]
    sum2 = list2[0] + list2[-1] if len(list2) > 1 else list2[0]
    
    # CHOOSE THE LARGER SUM
    result = sum1 if sum1 > sum2 else sum2
    result = "Same" if sum1 == sum2 else result
    
    print("Output:", result)
