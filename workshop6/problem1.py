sentence = None
while(sentence != ""):
    # GET INPUT AND CONVERT INTO LOWER CASE
    sentence = input("Enter a string:").lower()

    # SORT EACH WORD IN LIST BY DESCENDING
    words = sorted(sentence.split(), reverse=True)

    # MEARGE ALL WORDS INTO STRING LINE
    words = " ".join(words)

    print("Output:", words)

