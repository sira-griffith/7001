# FUNCTION FOR CHECKING g IN EACH CHARACTOR IN A WORD
def isHappy(word):
    #SINGLE CHARATOR INPUT WILL BE False
    if len(word) < 2:
        return False
    #STRING LINE INPUT WILL SEARCHING FOR gg IN LOOP
    for char in word:
        # SEARCHING FOR g AND INDEX MUST < LENGTH OF STRING
        if (char == "g") and (word.index(char) < len(word) - 1):
            # SEARCHING FOR NEXT g IN NEXT CHARATOR
            return True if word[word.index(char) + 1] == "g" else False

# STARTING PROGRAM
while(True):
    word = input("String: ")
    print("Happy?:", isHappy(word))
