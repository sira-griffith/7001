# GET INPUT FOR EACH STUDENT AND CREATE 2D ARRAY
students = [
            [int(num) for num in input("Student 1 (courses 1-4): ").split()],
            [int(num) for num in input("Student 2 (courses 1-4): ").split()],
            [int(num) for num in input("Student 3 (courses 1-4): ").split()],
            [int(num) for num in input("Student 4 (courses 1-4): ").split()],
            [int(num) for num in input("Student 5 (courses 1-4): ").split()]
        ]

# SUMATION AND FIND HIGHTEST AVG FROM EACH ROW
def calHighestAvg(table):
    lengthOfCol = len(table[0])
    sumEachRow = [sum(row) for row in table]
    avgEachRow = [float(number) / lengthOfCol for number in sumEachRow]
    return sorted(avgEachRow, reverse=True)[0]

# ROTATE A CURRENT TABLE FROM X,Y INTO Y,X
def rotate(table, x, y):
    rotatedTable = []
    for row in range(x):
        line = []
        for col in range(y):
            line.append(table[col][row])
        rotatedTable.append(line)
    return rotatedTable

# STARTING CALCULATE HIGHTEST AVG OF STUDENT
hightestAvgOfStudent = calHighestAvg(students)
print("The highest average mark of students:", hightestAvgOfStudent)

# ROTATING AND STARTING CALCULATE HIGHTEST AVG OF COURSE
courses = rotate(students, 4, 5)
hightestAvgOfCourse = calHighestAvg(courses)
print("The highest average mark of courses:", hightestAvgOfCourse)
