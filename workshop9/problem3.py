import math

# CORE PROGRAM
def main():
    # GET LOCATIONS
    locations = getInput()
    # CHECKING INPUT FORMAT; INCORRECT TERMINATE PROGRAM
    if checkInput(locations) == False:
        exit()
    # CONVERT THE AXISE THE CALCULATE FORM
    locations = [adjustInput(location) for location in locations]
    # ACCUMULATE THE DISTANCE OF COUPLE LOCATIONS
    calLocations = []
    # LOOPING FOR CALCULATE WHOLE TRIPS SEQUENCE
    for index in range(len(locations) - 1):
        calLocations += [calDistance(locations[index], locations[index + 1])]
    # CALCULATE DISTANCE
    dist = calGrid(sum(calLocations), 0.5)
    print("Total distance = {:.1f}".format(dist), "km")

# CALCULATE DISTANCE WITH GRID LINE
def calGrid(locatSumDist, gridDist):
    return locatSumDist * gridDist

# CALCULATE DISTANCE BETWEEN 2 LOCATION; [X, Y]
def calDistance(locat1, locat2):
    distX = (locat2[0] - locat1[0]) ** 2
    distY = (locat2[1] - locat1[1]) ** 2
    return math.sqrt(distX + distY)

# ADJUST INPUT FOR SUTABLE CALCULATE, CONVERT X AXIS TO NUMBER
def adjustInput(axises):
    X = Xaxis[axises[0]]
    Y = int(axises[1:])
    return [X, Y]

# GET INPUT FROM KEYBOARD
def getInput():
    return input("Enter trip map references: ").split()

# CHECK INPUT FORMAT
def checkInput(locations):
    syntax = 0
    badRef = []
    # LOOP CHECK ALL LOCATIONS
    for location in locations:
        # CHECKING LENGTH
        if len(location) < 2:
            syntax += 1
            badRef.append(location)
        else:
            # CHECKING UPPER CASE
            if location[0].isupper():
                num = int(location[1:])
                # CHECKING BOUNDARY
                if num < 1 or num >26:
                    syntax += 1
                    badRef.append(location)
            else:
                syntax += 1
                badRef.append(location)
    # PRINT BAD REF IF EXIST
    if len(badRef) > 0:
        print("Bad reference: ", * badRef)
    # RETURN BOOLEAN TRUE IF INPUT CORRECTED
    return syntax == 0

# SHOW MAPPING VALUE OF X AXIS
def showXAxis():
    for i in Xaxis:
        print(i + "-" +str(Xaxis[i]))

if __name__ == "__main__":
    # TABLE FOR MAPPING NUMBERS, A-Z
    Xaxis ={
            "A":1, "B":2, "C":3, "D":4, "E":5, "F":6, "G":7, "H":8,
            "I":9, "J":10, "K":11, "L":12, "M":13, "N":14, "O":15,
            "P":16, "Q":17, "R":18, "S":19, "T":20, "U":21, "V":22,
            "W":23, "X":24, "Y":25, "Z":26
    }
    main()
