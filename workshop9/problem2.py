# CORE PROCESS OF REPL
def main():
    print(_CONF["INITIAL-TEXT"])
    #  RECIVED COMMAND FROM FILE
    _INSTR_FILE = getCMDfromFile()
    for R_CMD in _INSTR_FILE:
        R_CMD = R_CMD.split()
        syntaxChecker(R_CMD)

# GETTING COMMAND FROM INPUT
def getCMDfromFile():
    # OPEN FILE NAME FROM USER INPUT
    sourceFile = open(input("Script name: "))
    # READING THE COMMAND FORM LINE AND CLEAN TEXT
    CMDS = [line.strip("\n") for line in sourceFile]
    # TERMINATE STREAMMING FILE
    sourceFile.close()
    return CMDS

# CHECKING SYNTAX
def syntaxChecker(CMD):
    # CHECK CMD quit
    if len(CMD) == 1:
        if CMD[0] == _INSTR[0]:
            selectCMD(CMD[0], CMD)
        else:
            errorCMD()
    # CHECK CMD print, input
    elif len(CMD) == 2:
        # ADJUST PARAMS FOR INSTRUCTION print
        if CMD[0] in _INSTR[1]:
            selectCMD(CMD[0], CMD[1])
        # ADJUST PARAMS FOR INSTRUCTION input
        elif CMD[0] in _INSTR[2]:
            selectCMD(CMD[0], CMD[1])
    # CHECK CMD get, adds
    elif len(CMD) == 3:
        # ADJUST PARAMS FOR INSTRUCTION get
        if CMD[1] in _INSTR[3]:
            if CMD[0].isdigit() == False:
                selectCMD(CMD[1], [CMD[0], CMD[2]])
            else:
                errorCMD()
        # ADJUST PARAMS FOR INSTRUCTION adds
        elif CMD[1] in _INSTR[4]:
            if CMD[0].isdigit() == False:
                selectCMD(CMD[1], [CMD[0], CMD[2]])
            else:
                errorCMD()
    else:
        errorCMD()

# SELECT THE INSTRUCTION TO EXECUTE CMD
def selectCMD(INSTR, CMD):
    # INSTRUCTION (INSTR)
    if INSTR == _INSTR[0]:
        CMD_Terminate()
    elif INSTR == _INSTR[1]:
        CMD_Print(CMD)
    elif INSTR == _INSTR[2]:
        CMD_Input(CMD)
    elif INSTR == _INSTR[3]:
        CMD_Gets(CMD[0], CMD[1])
    elif INSTR == _INSTR[4]:
        CMD_Adds(CMD[0], CMD[1])

# PRINT ERROR MESSAGE
def errorCMD():
    print("Syntax error.")

# CMD FOR TERMINATED PROGRAM
def CMD_Terminate():
        print("Goodbye.")
        exit()

# CMD FOR DEFINE INPUT FOR VARIABLE
def CMD_Input(VAR):
    VAL = input("Enter a value for " + VAR + ": ")
    _MEMORY[VAR] = int(VAL)

# CMD FOR GET, DEFINING VALUE FOR VARIABLE
def CMD_Gets(VAR1, VAR2):
    # DEFINE VARIABLE VAR1 IF NOT DEFINED
    if VAR1 not in _MEMORY:
        _MEMORY[VAR1] = ""
    if VAR2.isdigit() == True:
        _MEMORY[VAR1] = int(VAR2)
    elif VAR2 in _MEMORY:
        _MEMORY[VAR1] = _MEMORY[VAR2]

# CMD FOR ADDS, PLUS A VALUE TO SPECIFIC VARIABLE
def CMD_Adds(VAR1, VAR2):
    # DEFINE VARIABLE VAR1 IF NOT DEFINED
    if VAR1 not in _MEMORY:
        _MEMORY[VAR1] = ""
    elif VAR2 in _MEMORY:
        _MEMORY[VAR1] += _MEMORY[VAR2]

# CMD FOR GET VALUE OF VARIABLE
def CMD_Print(VAR):
    # CHECK VARIABLE SHOULD NOT BE INTEGER
    if VAR.isdigit() == False:
        # CHECK VARIABLE STORAGE IN MEMORY
        if VAR in _MEMORY:
            print(VAR, "equals", _MEMORY[VAR])
        else:
            print(VAR, "is undefined")
    # PRINT OUT THE NUMBER IF VAR IS NOT VARIABLE
    else:
        print(VAR)

if __name__ == "__main__":
    # REPL CONFIGURATION
    _CONF = {
            "INITIAL-TEXT": "Welcome to the Adder REPL. (PROBLEM 2)"
            }
    # INSTRUCTION LISTS
    _INSTR = ("quit", "print", "input", "gets", "adds")
    # INSTRUCTION FROM FILE
    _INSTR_FILE = []
    # STORAGE ALL VARIABLE OF REPL
    _MEMORY = {}
    main()
