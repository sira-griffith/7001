# CORE PROCESS OF REPL
def main():
    print(_CONF["INITIAL-TEXT"])
    # CYCLING PROGRAM
    while True:
        #  RECIVED COMMAND AS R_CMD
        R_CMD = getCMD()
        syntaxChecker(R_CMD)

# GETTING COMMAND FROM INPUT
def getCMD():
    return input(_CONF["SHELL"]).split()

# CHECKING SYNTAX
def syntaxChecker(CMD):
    # CHECK CMD quit
    if len(CMD) == 1:
        if CMD[0] == _INSTR[0]:
            selectCMD(CMD[0], CMD)
        else:
            errorCMD()
    # CHECK CMD print, input
    elif len(CMD) == 2:
        # ADJUST PARAMS FOR INSTRUCTION print
        if CMD[0] in _INSTR[1]:
            selectCMD(CMD[0], CMD[1])
        # ADJUST PARAMS FOR INSTRUCTION input
        elif CMD[0] in _INSTR[2]:
            selectCMD(CMD[0], CMD[1])
    # CHECK CMD get, adds
    elif len(CMD) == 3:
        # ADJUST PARAMS FOR INSTRUCTION get
        if CMD[1] in _INSTR[3]:
            if CMD[0].isdigit() == False:
                selectCMD(CMD[1], [CMD[0], CMD[2]])
            else:
                errorCMD()
        # ADJUST PARAMS FOR INSTRUCTION adds
        elif CMD[1] in _INSTR[4]:
            if CMD[0].isdigit() == False:
                selectCMD(CMD[1], [CMD[0], CMD[2]])
            else:
                errorCMD()
    else:
        errorCMD()

# SELECT THE INSTRUCTION TO EXECUTE CMD
def selectCMD(INSTR, CMD):
    # INSTRUCTION (INSTR)
    if INSTR == _INSTR[0]:
        CMD_Terminate()
    elif INSTR == _INSTR[1]:
        CMD_Print(CMD)
    elif INSTR == _INSTR[2]:
        CMD_Input(CMD)
    elif INSTR == _INSTR[3]:
        CMD_Gets(CMD[0], CMD[1])
    elif INSTR == _INSTR[4]:
        CMD_Adds(CMD[0], CMD[1])

# PRINT ERROR MESSAGE
def errorCMD():
    print("Syntax error.")

# CMD FOR TERMINATED PROGRAM
def CMD_Terminate():
        print("Goodbye.")
        exit()

# CMD FOR DEFINE INPUT FOR VARIABLE
def CMD_Input(VAR):
    VAL = input("Enter a value for " + VAR + ": ")
    _MEMORY[VAR] = int(VAL)

# CMD FOR GET, DEFINING VALUE FOR VARIABLE
def CMD_Gets(VAR1, VAR2):
    # DEFINE VARIABLE VAR1 IF NOT DEFINED
    if VAR1 not in _MEMORY:
        _MEMORY[VAR1] = ""
    if VAR2.isdigit() == True:
        _MEMORY[VAR1] = int(VAR2)
    elif VAR2 in _MEMORY:
        _MEMORY[VAR1] = _MEMORY[VAR2]

# CMD FOR ADDS, PLUS A VALUE TO SPECIFIC VARIABLE
def CMD_Adds(VAR1, VAR2):
    _MEMORY[VAR1] += _MEMORY[VAR2]

# CMD FOR GET VALUE OF VARIABLE
def CMD_Print(VAR):
    # CHECK VARIABLE SHOULD NOT BE INTEGER
    if VAR.isdigit() == False:
        # CHECK VARIABLE STORAGE IN MEMORY
        if VAR in _MEMORY:
            print(VAR, "equals", _MEMORY[VAR])
        else:
            print(VAR, "is undefined")
    # PRINT OUT THE NUMBER IF VAR IS NOT VARIABLE
    else:
        print(VAR)

if __name__ == "__main__":
    # REPL CONFIGURATION
    _CONF = {
            "SHELL": "??? ",
            "INITIAL-TEXT": "Welcome to the Adder REPL."
            }
    # INSTRUCTION LISTS
    _INSTR = ("quit", "print", "input", "gets", "adds")
    # STORAGE ALL VARIABLE OF REPL
    _MEMORY = {}
    main()
