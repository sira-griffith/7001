positiveCounter = 0
inputNum = None

while inputNum != 0:
    inputNum = int(input("Enter a number: "))
    if inputNum > 0:
        positiveCounter += 1

print(positiveCounter, "positive numbers were entered.")
