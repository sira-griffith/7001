loop = int(input("Enter a positive number: "))
grideSize = 2 * loop - 1
sideSize = int((grideSize - 1) / 2)

# Top part
i = 0
while i < loop:
    left = " " * (sideSize - i) + "x" * i
    right = "x" * i + " " * (sideSize - i)
    i += 1
    print(left + "x" + right)

# Bottom Part
i = loop - 2
while i >= 0:
    left = " " * (sideSize - i) + "x" * i
    right = "x" * i + " " * (sideSize - i)
    i -= 1
    print(left + "x" + right)
