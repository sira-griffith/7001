# -------------------------------------------------
# Initial: F1 = 0, F2 = 1
# Concept: Fn = F[n-1] + F[n-2]
# -------------------------------------------------
# Illustration idea:
#       F1              F2              Fn      
#   +-----------+   +-----------+   +-------------+
#   | F_twoPrev |   | F_onePrev |   | F_currently |
#   +-----------+   +-----------+   +-------------+

loop = int(input("Enter a positive number: "))
rowLimit = 4
rowCounter = 1

F_onePrev = 0
F_twoPrev = 0
result = ""

i = 0
while i < loop:
    # Force set suitable value F1=0, F2=1 in loop3
    F_onePrev = 1 if i == 1 else F_onePrev
    F_twoPrev = 0 if i == 2 else F_twoPrev

    # Cal current Fibo sequence
    F_currently = F_onePrev + F_twoPrev
    result += str(F_currently) + " "
    # Switch box result
    F_twoPrev = F_onePrev
    F_onePrev = F_currently

    # Check new line
    if rowCounter == rowLimit:
        result += "\n"
        rowCounter = 0

    rowCounter += 1
    i += 1
print(result)
