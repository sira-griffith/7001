# MAIN FUNCTION
def main():
    # CALLING FUNC TO GET DATA FROM SOURCE FILE
    numbers = getDataFromFile()
    # START TO LOOP FOR CHECK ALL LIST NUMBERS
    for rowNumber in numbers:
        # CALLING FUNC TO CHECK SUCCESSIVE NUMBER
        result = isSuccessiveNumber(rowNumber)
        #PRINT OUT THE RESULT ALSO EXTRACT THE LIST TO STRING
        print(*rowNumber, result)

# CHECK SUCCESSIVE NUMBER FROM LIST WHICH CONTAINS ONLY 4 ITEMS
def isSuccessiveNumber(numbers):
    # CONVERT NUMBER INTO INTEGER TYPE
    numbers = [int(num) for num in numbers]
    # CHOP INTO 2 LISTS: left, right
    # right NEED TO REVERSE ITEM FOR CONVEINENCE CAL BY INDEX
    left = numbers[:2]
    right = numbers[2:][::-1]
    # RETURN bool; THE RESULT BETWEEN 2 INDEX NEED TO BE EQUAL
    return (left[0] + right[0]) == (left[1] + right[1])

# OPEN FILE BY NAME FROM USER INPUT
def getDataFromFile():
    # OPEN FILE NAME FROM USER INPUT
    sourceFile = open(input("File name: "))
    # READING FILE FOR EACH LINE, COVERT A LINE TO LIST
    data = [line.split() for line in sourceFile]
    # TERMINATE STREAMMING FILE
    sourceFile.close()
    # RETURN DATA FROM OPENED FILE
    return data

# SET MODULE EXECUTION ON main FUNCTION FIRST
if __name__ == "__main__":
    main()
