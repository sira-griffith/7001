# GET INPUT AND CONVERT INTO INTEGER LIST
lists = [int(num) for num in input("Input a list: ").split()]
# DEFINE LENGTH OF LIST
lenList = len(lists)

# LOOP FOR ROTATE THE INDEX; +1 FOR LENGTH FOR INITIAL FORM
for index in range(lenList + 1):
    # CHECK IF index < LENGTH THEN ROTATE INDEX
    # ELSE KEEP PRINTING INITIAL FORM
    if (index < lenList):
        # CHOP THE A LIST INTO 2 CHUNKS
        # FIRST CHUNK INDEX STARTS FROM LOOP'S INDEX
        # SECOND CHUNK IS REMAINING
        print(lists[index:] + lists[:index])
    else:
        print(lists)
