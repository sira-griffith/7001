while True:
    # GET INPUT AND CONVERT INTO INTEGER LIST
    list1 = [int(num) for num in input("List 1: ").split()]
    list2 = [int(num) for num in input("List 2: ").split()]
    # CACHE VARIABLE FOR STORAGE SAME  NUMBER
    sameList = []
    
    for num1 in list1:
        for num2 in list2:
            # PREPASE TO INSERT INTO sameList IF BOTH NUM ARE SAME
            if num2 == num1:
            # CHECK IF sameList NO ITEM IN LIST THEN INSERT STRAIGHT ALWAY
                if len(sameList) > 0:
                    #CHECKING PREVENT DUPLICATE NUM INSERT TO LIST
                    if not num2 in sameList:
                        sameList.append(num2)
                else:
                    sameList.append(num2)
    # PRINT OUT THE RESULT
    print(sameList)
