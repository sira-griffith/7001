# ========== TABLE CONTEXT OF SUB FUNCTIONS ==========
# [FUNC-1]              getInfectedIndex
# [FUNC-1.1]            searchFirstInfection
# [FUNC-1.2]            searchNextInfection
# [FUNC-2]              getInfectedMembers
# [FUNC-2.1]            getInfectedMembersSingleIndex
# [FUNC-2.2]            getInfectedMembersDoubleIndex
# [FUNC-3]              generateInfectedRecord
# [FUNC-3.1]            generateCopiedRecord
# [FUNC-5]              getDataFromFile
# =====================================================
# NOTE INPUT FORMAT ONLY meetings1.txt, meetings2.txt
# =====================================================
def main():
    # GET RECORD DATA FROM SOURCE FILE; [FUNC-5]
    records = getDataFromFile()
    # CACHING INFECTION RECORDS
    infectedRecords = []
    # SCANING INFECTED BOOK FEVER BY LOOPING THE RECORD
    for record in records:
        members = record[1:]
        # GET INDEX OF INFECTED MEMBER; [FUNC-1]
        indexOfInfectedMemeber = getInfectedIndex(members, infectedRecords)
        # SET FIRST CASE "True" IF NO ONE INFECTE
        isFirstCase = True if len(infectedRecords) == 0 else False
        # HAVE INFECTION
        if len(indexOfInfectedMemeber) > 0:
            # GET NAME OF INFECTED MEMBER; [FUNC-2]
            infectedMember = getInfectedMembers(indexOfInfectedMemeber, members, isFirstCase)
            # MAKE NEW RECORD OF FOUND INFECTED CASE; [FUNC-3]
            newInfectedRecord = generateInfectedRecord(record[0], infectedMember, infectedRecords)
            infectedRecords.append(newInfectedRecord)
        # NO INFECTION BUT CASE BEFORE HAS, CONTINUE ACCUMULATE RECORD
        elif len(indexOfInfectedMemeber) == 0 and not isFirstCase:
            # ACCUMULATE RECORD BY COPY INFO OF LASTEST INFECTED CASES; [FUNC-3.1]
            infectedRecords.append(generateCopiedRecord(infectedRecords[-1]))
    # PRINT OUT RESULT FROM INFECTED RECORDS; [FUNC-4]
    printReult(infectedRecords)

# [FUNC-1] ORAGANISE GET INDEX FUNC
def getInfectedIndex(members, infectedList):
    if len(infectedList) > 0:
        # GET INDEX IF NOT FIST CASE; [FUNC-1.2]
        return searchNextInfection(members, infectedList[-1])
    else:
        # GET INDEX FIRST CASE FOUND; [FUNC-1.1]
        return searchFirstInfection(members, "*")

# [FUNC-2] ORAGANISE GET NAME FUNC
def getInfectedMembers(index, members, isFirstCase):
    if len(index) > 1:
        # GET INFECTED MEMBER IF INDEX CONSIS 2 KEYS; [FUNC-2.1]
        return getInfectedMembersDoubleIndex(index, members, isFirstCase)
    else:
        # GET INFECTED MEMBER IF INDEX CONSIST 1 KEY; [FUNC-2.2]
        return getInfectedMembersSingleIndex(index, members, isFirstCase)

# [FUNC-1.1] SEARCH INDEX FOR FIRST CASE INFECTION
def searchFirstInfection(members, sign):
    # CACHING MEMBER INDEX
    infectedIndex = []
    # GET NAME OF INFECTED MEMBER
    target = [name for name in members if sign in name]
    # GET INDEX OF INFECTED MEMBER, ELSE WILL RETURN []
    if len(target) > 0:
        infectedIndex.append(members.index(target[0]))
    return infectedIndex

# [FUNC-1.2] SEARCH INDEX FOR AFTER FIRST CASE INFECTION
def searchNextInfection(members, lastestRecord):
    # CACHING MEMBER INDEX
    infectedIndex = []
    # ADJUST INFECTED RECORD BY GRAB ONLY MEMBER NAME
    lastestInfectedMembers = lastestRecord[1:-1]
    # START SEARCHING INFECTED MEMBER BY COMPARE WITH NAME LIST IN INFECTED RECORD
    for infected in lastestInfectedMembers:
        # GET INDEX OF INFECTED MEMBER
        target = [members.index(member) for member in members if infected in member]
        # ADD FOUND INDEX INTO RETURN LIST, ELSE WILL RETURN []
        infectedIndex += target if len(target) > 0 else infectedIndex
    return infectedIndex

# [FUNC-2.1] GET INFECTED MEMBER BY 1 INDEX KEY
def getInfectedMembersSingleIndex(index, members, isFirstCase):
    result = []
    index = index[0]
    # FORCE ADDING FIRST INFECTED MEMBER
    if isFirstCase:
        result.append(members[index][0:-1])
    # GET INFECTED MEMBER WHO IS SITTING BETWEEN INDEX > [0] AND < [-1]
    if index > 0 and index < len(members) - 1:
        result += [members[index - 1], members[index + 1]]
    else:
        # GET INFECTED MEMBER WHO IS SETTING AT INDEX [0] OR [-1]
        if index == 0:
            result += [members[index + 1], members[-1]]
        else:
            result += [members[0], members[index - 1]]
    return result

# [FUNC-2.2] GET INFECTED MEMBER BY 2 INDEX KEYS
def getInfectedMembersDoubleIndex(index, members, isFirstCase):
    result = []
    # DEFINE SEAT LOCATION OF 2 INFECTED MEMBERS
    left = index[0]
    right = index[1]
    # GET INFECTED MEMBERS WHO ARE SITTING BETWEEN INDEX > [0] AND < [-1]
    if 0 not in index and len(members) - 1 not in index:
        result += [members[left - 1], members[right + 1]]
    else:
        # GET INFECTED MEMBERS WHO ARE SETTING AT INDEX [0] OR [-1]
        if 0 in index:
            result += [members[right + 1], members[-1]]
        else:
            result += [members[0], members[left - 1]]
    return result

# [FUNC-3] GENERATING, ADJUSTING INFECTED RECORD
def generateInfectedRecord(day, members, infectedRecord):
    # NOT FIRST RECORD
    if len(infectedRecord) > 0:
        # GET LASTEST INFECTED RECORD
        infectedRecord = infectedRecord[-1]
        # GET INFECTED NAME FROM PREVIOUS RECORD
        newRecord = infectedRecord[1:-1]
        # ADD NEW FOUND INFECTED NAME TO RECORD
        newRecord += members
        # ADJUST FORMAT - [DATE, INFECTED_MEMBERS, TOTAL_CASE]
        result = [day]
        result += newRecord
        result += [len(newRecord)]
        return  result
    # FIRST RECORD
    else:
        # ADJUST FORMAT - [DATE, INFECTED_MEMBERS, TOTAL_CASE]
        result = [day]
        result += members
        result += [len(members)]
        return result

# [FUNC-3.1] GENERATING, ADJUSTING A COPY OF INFECTED RECORD
def generateCopiedRecord(record):
    # DEFINE DATE NUMBER
    copiedRecord = [str(int(record[0]) + 1)]
    # COPYING PREVIOUS RECORD DEATIL
    copiedRecord += record[1:]
    return copiedRecord

# [FUNC-4] PRINTING, FORMATING OUTPUT
def printReult(records):
    for record in records:
        print(*record, end='\n')

# [FUNC-5] OPEN FILE BY NAME FROM USER INPUT
def getDataFromFile():
    # OPEN FILE NAME FROM USER INPUT
    sourceFile = open(input("Enter file name: "))
    # READING FILE FOR EACH LINE, COVERT A LINE TO LIST
    data = [line.split() for line in sourceFile]
    # TERMINATE STREAMMING FILE
    sourceFile.close()
    # RETURN DATA FROM OPENED FILE
    return data

# SET MODULE EXECUTION ON main FUNCTION FIRST
if __name__ == "__main__":
    main()
