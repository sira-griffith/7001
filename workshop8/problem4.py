# MAIN FUNCTION
def main():
    # GET INPUT AND CONVERT INTO INTEGER LIST
    list1 = [int(num) for num in input("List 1: ").split()]
    list2 = [int(num) for num in input("List 2: ").split()]
    # CALLING FUNC TO MERGES BOTH LIST
    result = merge(list1, list2)
    # PRINT OUT THE RESULT
    print(result)

# MEARGE
def merge(list1, list2):
    lists = list1 + list2
    mergeSortDesc(lists)
    return lists

# SUB PROCESS OF MERGE FUNC WILL SORT AND ARRANGES IN DESC
def mergeSortDesc(lists):
    # LIST FROM PARAM NEED TO CONTAIN MORE THAN 1 ITEM
    # FOR SPLIT INTO 2 PART LEFT RIGHT
    if len(lists) > 1:
        mid = len(lists) // 2
        left = lists[:mid]
        right = lists[mid:]

        # CALL RECURSIVE FUNC TO DO SAME PROCESS
        mergeSortDesc(left)
        mergeSortDesc(right)

        # KEY: i = left side, j = right side, k = entire chunk
        i, j, k = 0, 0, 0

        # HAVE 2 CHUNKS LEFT, RIGHT ABLE TO COMPARE
        while i < len(left) and j < len(right):
            if left[i] > right[j]:
                lists[k] = left[i]
                i += 1
            else:
                lists[k] = right[j]
                j += 1
            k += 1

        # HAVE SINGLE CHUNK ONLY LEFT SIDE
        while i < len(left):
            lists[k] = left[i]
            i += 1
            k += 1

        # HAVE SINGLE CHUNK ONLY RIGHT SIDE
        while j < len(right):
            lists[k] = right[j]
            j += 1
            k += 1

# SET EXECUTION ON main FUNCTION FIRST
if __name__ == "__main__":
    while True:
        main()
