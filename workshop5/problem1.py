# GET INPUT AS A STRING
word = str(input("Enter a string: "))
# SET EMPLY VARIABLE FOR STORAGE SHORTEST WORD
shortest = None

# SET LOOP TO KEEP PROGRAM ASKING FOR NEXT INPUT UNTIL INPUT CONSIST A AT FIRST LETTER
while word[0] != "A":
    # SET FOR FIRST word HAD DEFINE
    shortest = word if shortest == None else shortest
    # CHECK IF NEW INPUT word SHORTER THAN shortest VARIABLE
    if len(word) < len(shortest): shortest = word
    word = str(input("Enter a string: "))
# PRINT RESULT
print(shortest)
