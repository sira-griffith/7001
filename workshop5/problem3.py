# GET INPUT FOR START YEAR AS INTEGER
startYear = int(input("Year 1? "))
# GET INPUT FOR END YEAR AS INTEGER
endYear = int(input("Year 2? "))

#CHECKING LEAP YEAR, FUNC checkLeapYear JUST RETURN BOOLEAN TRUE | FALSE
def checkLeapYear(year):
    # SET DEFAULT RETURN AS FALSE
    result = False
    # CHECKING LEAP YEAR AND CHANGEING RETURN RESULT AS TRUE; 
    # IF year % 4, year % 100 MUST GET 1 OR year % 400 MUST GET 0
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        result = True
    return result

# SET VARIABLE FOR ACCUMURATE NUMBER OF DAYS BETWEEN START, END YEARS
numberOfday = 0
# SET LOOP FOR THE TIME PERIOD, PLUS 1 FOR SUTABLE LOOP BECAUSE range IS USING INDEX NUMBER OR ARRAY
for year in range(startYear, endYear + 1):
    # CALLING FUNC FOR CHECK LEAP YEAR, IF LEAP YEAR +366 OTHERWISE +365 NORMAL YEAR
    if checkLeapYear(year):
        numberOfday += 366
    else:
        numberOfday += 365

# PRINT RESULT
print("Number of days: " + str(numberOfday))
