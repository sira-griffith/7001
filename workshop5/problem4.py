# GET INPUT FOR BOTH DIAMENTION AS FLOAT
dimension1 = float(input("Enter room dimension 1 (m): "))
dimension2 = float(input("Enter room dimension 2 (m): "))

# FUNC calLengthReq WILL CALCULATE LENGTH REQUIRED SIZE
# IT NEED 3 PARAMS roomSize = AREA, carpet = CARPET WIDTH, 
# dimension = SHAPE OF DIMENSION ACCORDING FROM THE GIVEN PICTURE
def calLengthReq(roomSize, carpet, dimension):
    # THE CONCEPT TO CALCULATE THE REQUIRE SIZE:
    # USING ROOM AREA - CARPET SIZE
    # FIND CAPET SIZE BY DIMENSION * CARPET WIDTH
    # USING ABSLUTE FUNC FOR GETTING POSITIVE NUMBER
    calRequireSize = abs(roomSize - (carpet * dimension))
    # USING round FUNC FOR HANDLE WITH FRACTION AND GET APPROXIMATE NUMBER
    return round(calRequireSize)

# LOOPING WHEN BOTH DIMENSIONS MORE THAN 0
while(dimension1 > 0 or dimension2 > 0):
    # DEFINE LENGTH AND WIDTH BY CHECKING
    # IF WHICH DIMENSION MORE THAN ANOTHER, THEN IT WILL BE LENGTH
    length = dimension1 if dimension1 > dimension2 else dimension2
    width = dimension1 if dimension1 < dimension2 else dimension2
    carpetWide = 3.66
    
    # STARTING CALCULATE REQUIRE SIZE
    # BY CALL calLengthReq WHICH NEED 3 PARAMS SUCH AS AREA, CARPET WIDTH AND DIAMENSION
    D1 = calLengthReq(float(length * width), carpetWide, length)
    D2 = calLengthReq(float(length * width), carpetWide, width)
    
    # PRINTING CALCULATED INFO
    # PRINTING LENGTH AND WIDTH BY FORMAT WITH 3 FRACTION
    print("Length = {:.3f}".format(length) + " m")
    print("Width = {:.3f}".format(width) + " m")
    # PRINTING TOTAL OF REQUIRED CARPET
    print("Total length required Lengthways = {:.0f}".format(D1) + " m")
    print("Total length required Widthways = {:.0f}".format(D2) + " m")
    
    #ASK FOR FURTHER INPUTS
    dimension1 = float(input("Enter room dimension 1 (m): "))
    dimension2 = float(input("Enter room dimension 2 (m): "))

