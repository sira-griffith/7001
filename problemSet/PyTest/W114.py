from PyTest import *
##/////////////// PROBLEM STATEMENT /////////////////
## Convert a height input as centimetres to metres //
## and centimetres                                 //
##                                                 //
##  centimetres     metres centimetres             //
##      110      ->    1       10                  //
##     1256      ->   12       56                  //
##///////////////////////////////////////////////////

height = int(input())
metres = height // 100
centimetres = height % 100
print(metres, ":", centimetres)
