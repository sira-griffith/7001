from PyTest import *
##///////////// PROBLEM STATEMENT /////////////
## Write Python code which, when it reads    // 
## two input boolean values, produces the    //
## following results:                        //
##                                           //
##   True True   -> True                     //
##   True False  -> False                    //
##   False True  -> False                    //
##   False False -> True                     //
##/////////////////////////////////////////////

x = input()
y = input()

if x == "True" and y == "True":
    print(True)
elif x == "False" and y == "False":
    print(True)
else:
    print(False)
