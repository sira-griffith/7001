from PyTest import *
##///////////// PROBLEM STATEMENT /////////////
## Write Python code which, when it reads    // 
## two input boolean values, produces the    //
## following results:                        //
##                                           //
##   True True   -> False                    //
##   True False  -> False                    //
##   False True  -> False                    //
##   False False -> True                     //
##/////////////////////////////////////////////

v1 = input()
v2 = input()

if (v1 == "True") and (v2 == "True"):
    print(False)
elif (v1 == "True") and (v2 == "False"):
    print(False)
elif (v1 == "False") and (v2 == "True"):
    print(False)
elif (v1 == "False") and (v2 == "False"):
    print(True)
else:
    print(True)

