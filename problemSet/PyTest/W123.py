from PyTest import *
##//////////////////// PROBLEM STATEMENT ////////////////////////
## Given a 24 hour time of day as hours minutes seconds, add   //
## a time interval which is specified as hours minutes seconds //
##                                                             //
##   hrs mins secs hrs mins secs    hrs mins secs              // 
##   13   24   30   2   40   40  -> 16    5   10               //
##///////////////////////////////////////////////////////////////

hr1 = int(input())
min1 = int(input())
sec1 = int(input())

hr2 = int(input())
min2 = int(input())
sec2 = int(input())

# temp accomulated total mins
tempCalMin = min1 + min2 + (sec1 / 60 + sec2 / 60)

totalHr = int(hr1 + hr2 + (tempCalMin // 60))
totalMin = int(tempCalMin % 60)
totalSec = int(tempCalMin // 60 * 10)

print(totalHr, totalMin, totalSec)
