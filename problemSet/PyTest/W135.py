from PyTest import *
##/////////////// PROBLEM STATEMENT ////////////////
## Given two temperatures, print True if one is   //
## less than 0 and the other is greater than 100. //
##   120, -1 -> True                              //
##   -1, 120 -> True                              //
##   2, 120 -> False                              //
##//////////////////////////////////////////////////

x = int(input())
y = int(input())

large = 0
small = 0

if x > y:
    large = x
    small = y
else:
    large = y
    small = x

if large > 100 and small < 0:
    print(True)
else:
    print(False)
