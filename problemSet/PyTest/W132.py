from PyTest import *
##///////////// PROBLEM STATEMENT /////////////
## Write Python code which, when it reads    // 
## two input boolean values, produces the    //
## following results:                        //
##                                           //
##   True True   -> False                    //
##   True False  -> False                    //
##   False True  -> True                     //
##   False False -> False                    //
##/////////////////////////////////////////////

x = input()
y = input()

if x == "True" and y == "True":
    print(False)
elif x == "True" and y == "False":
    print(False)
elif x == "False" and y == "True":
    print(True)
else:
    print(False)
