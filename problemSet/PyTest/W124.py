from PyTest import *
##/////////////////// PROBLEM STATEMENT /////////////////////////
## Given a 12 hour time of day as hours minutes seconds pm,    //
## add a time interval which is specified as hours minutes     //
## seconds. The input pm is 0 for morning and 1 afternoon.     //
##                                                             //
##   hrs mins secs am/pm  hrs mins secs    hrs mins secs am/pm //
##    1   24   30    1     2   40   40  ->  4   5    10    1   //
##///////////////////////////////////////////////////////////////



hr1 = int(input())
min1 = int(input())
sec1 = int(input())

ampm = int(input())

hr2 = int(input())
min2 = int(input())
sec2 = int(input())

# temp accomulated total mins
tempCalMin = min1 + min2 + (sec1 / 60 + sec2 / 60)

totalHr = int(hr1 + hr2 + (tempCalMin // 60))
totalMin = int(tempCalMin % 60)
totalSec = int(tempCalMin // 60 * 10)

print(totalHr, totalMin, totalSec, ampm)
