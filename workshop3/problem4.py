hours = int(input("How many hours were worked? "))
carSold = int(input("Total number of cars sold for the week? "))
wage = 36.25
hoursWorkWeek = 37
wagePlusExtar = 0
bonusCarSold = 0

if hours > hoursWorkWeek:
    extarPaid = 1.5
    hoursOvertime = hours - hoursWorkWeek
    hours = hoursWorkWeek
    wagePlusExtar = hoursOvertime * (wage * extarPaid)

if carSold > 5:
    bonusPerCar = 200
    extarCarSold = carSold - 5
    bonusCarSold = 200 * extarCarSold

totalSalary = (wage * hours) + wagePlusExtar + bonusCarSold
print("The salary is ", totalSalary)
