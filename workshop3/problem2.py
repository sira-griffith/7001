baseprice = float(input("How much is the base price? "))
weight = int(input("What is the weight? "))
distance = int(input("What is the distance? "))
cost = None

def estimateCost(baseprice, weight, distance, discountPercent):
    price = baseprice * weight * distance
    discount = (price * discountPercent) / 100
    return price - discount

if distance >= 3000:
    discount = 50
    cost = estimateCost(baseprice, weight, distance, discount)
elif distance < 3000 and distance >= 2000:
    discount = 35
    cost = estimateCost(baseprice, weight, distance, discount)
elif distance < 2000 and distance >= 1000:
    discount = 20
    cost = estimateCost(baseprice, weight, distance, discount)
elif distance < 1000 and distance >= 500:
    discount = 15
    cost = estimateCost(baseprice, weight, distance, discount)
elif distance < 500 and distance >= 250:
    discount = 10
    cost = estimateCost(baseprice, weight, distance, discount)
else:
    discount = 0
    cost = estimateCost(baseprice, weight, distance, discount)

print("The shipping cost is ", cost)
