lists = [
    int(input("Integer 1? ")),
    int(input("Integer 2? ")),
    int(input("Integer 3? "))
]

length = len(lists)

for i in range(length):
    if i + 1 < length:
        for j in range(i + 1, length):
            if lists[i] < lists[j]:
                tmp = lists[j]
                lists[j] = lists[i]
                lists[i] = tmp

listsToStr = ""
for i in lists:
    listsToStr = listsToStr +" "+ str(i)

print("Sorted:", listsToStr)

