mark = int(input("How many marks? "))
grade = None
if mark >= 85:
    grade = 7
elif mark < 85 and mark >= 75:
    grade = 6
elif mark < 75 and mark >= 60:
    grade = 5
elif mark < 60 and mark >= 40:
    grade = 4
else:
    grade = "F"

print(grade)
