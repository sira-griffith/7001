# Separating file for Account Class
class Account:
    # set properties
    logS = [["event", "amount ($)", "balance ($)"]]
    balance = 0
    initBa = 0

    # set constructor define intial account value
    def __init__(self, balance):
        self.initBa = balance
        self.balance = balance

    # minus
    def minus(self, num):
        self.balance -= num
        self.formating("Ride", num, self.balance)

    # adding balance
    def add(self, num):
        self.balance += num
        self.formating("Top up", num, self.balance)

    # get balance
    def getBalance(self):
        return self.balance

    # get statement
    def statement(self):
        providedStatement = self.logS
        providedStatement.append(
                    ["Final balance", "", "{:.2f}".format(self.balance)]
                )
        return providedStatement

    # formatting the statement
    def formating(self, type, amount, balance):
        # create initail heading
        if len(self.logS) == 1:
            self.logS.append(
                        ["Initial balance", "", "{:.2f}".format(self.initBa)]
                    )
        # adding middle part of statement
        self.logS.append(
                    [type, "{:.2f}".format(amount), "{:.2f}".format(balance)]
                )
