# Problem 1 no print statement
# Import Account Class
from Account import Account

def main():
    # Create the Go Card account
    GoCard = Account(
                float(input("Create account. Input initial balance: "))
            )

    while True:
        # Asking input from user to do menu
        menu = input("? ").split()

        # select r menu
        if menu[0] == "r" and len(menu) == 2:
            GoCard.minus(float(menu[1]))

        # select r menu
        elif menu[0] == "t" and len(menu) == 2:
            GoCard.add(float(menu[1]))

        # select b menu
        elif menu[0] == "b" and len(menu) == 1:
            print("Balance = ${:.2f}".format(GoCard.getBalance()))

        # select q menu
        elif menu[0] == "q" and len(menu) == 1:
            break
        else:
            print("Bad command.")

if __name__ == "__main__":
    main()
