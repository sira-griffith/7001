# Problem 2 able to print statement
# Import Account Class
from Account import Account

def main():
    # Create the Go Card account
    GoCard = Account(
                float(input("Create account. Input initial balance: "))
            )

    while True:
        # Asking input from user to do menu
        menu = input("? ").split()

        # select r menu
        if menu[0] == "r" and len(menu) == 2:
            GoCard.minus(float(menu[1]))

        # select r menu
        elif menu[0] == "t" and len(menu) == 2:
            GoCard.add(float(menu[1]))

        # select b menu
        elif menu[0] == "b" and len(menu) == 1:
            print("Balance = ${:.2f}".format(GoCard.getBalance()))

        # select q menu
        elif menu[0] == "q" and len(menu) == 1:
            statement = GoCard.statement()
            colWidth = 20
            print("Statement:")
            for record in statement:
                statementNote = record[0].ljust(colWidth) + record[1].ljust(colWidth) + record[2].ljust(colWidth)
                print(statementNote)
            # Stop the program when finish print statement
            break
        else:
            print("Bad command.")

if __name__ == "__main__":
    main()
