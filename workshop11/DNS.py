class DNS:
    # DEFINING A ROUTING TABLE AS PRIVATE PROPERTY
    # Eg. __ROUTING = {"127.0.0.1": ["localhost"]}
    __ROUTING = {}

    # UPDATING A NEW ROUTING RECORD
    def update(self, IPA, domainList):
        self.__ROUTING[IPA] = domainList

    # ADDING A NEW DOMAIN INTO EXIST RECORD
    def append(self, IPA, domain):
        self.__ROUTING[IPA].append(domain)

    # DELETING DOMAIN NAME FROM EXIST RECORD
    def deleteIP(self, IPA, domain):
        self.__ROUTING[IPA].remove(domain)

    # DELETING ENTIRE INFO FROM ROUTING RECORD
    def deleteDomain(self, IPA):
        del self.__ROUTING[IPA]

    # GETTING ALL IPA FROM ROUTING RECORD
    def getIPA(self):
        return self.__ROUTING.keys()

    # GETTING ALL DOMAIN FROM EXIST RECORD
    def getDomain(self, IPA):
        return self.__ROUTING[IPA] if IPA in self.__ROUTING.keys() else None
