def classRoomCal(bigRoom, smallRoom, eachClassStudent):
    return ((bigRoom*45) + (smallRoom*22)) // eachClassStudent

numberOfStudentEachClass = 25
numberOfBigRoom = int(input("How many big classrooms? "))
numberOfSmallRoom = int(input("How many small classrooms? "))

result = classRoomCal(numberOfBigRoom, numberOfSmallRoom, numberOfStudentEachClass)
print("Number of classes = ", result)
