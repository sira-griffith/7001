def concreteNeededCal(area, concretePerSquare):
    rectangularArea = area['width'] * area['length']
    return rectangularArea * concretePerSquare

areaLength = float(input("Length of park (m): "))
areaWidth = float(input("Width of park (m): "))
valueOfConcretePerSquare = float(input("Litres per square metre: "))

result = concreteNeededCal(
        area = {
            "width": areaWidth ,
            "length": areaLength },
        concretePerSquare = valueOfConcretePerSquare)

print("Litres required = ", result)
