# DEFINE 3 MAIN OUTPUTS
characts, words, lines = 0, 0, 0

# OPEN FILE NAME FROM USER INPUT
sourceFile = open(input("File name: "))
# READING FILE AND GRAB THE WHOLE TEXT
text = [line for line in sourceFile]
# TERMINATE STREAMMING FILE
sourceFile.close()

# COUNT NUMBER OF LINE FROM THE LENGTH OF LIST
lines = len(text)
# COUNT NUMBER OF WORD FROM EACH LINE BY TRIM OUT THE WHITE SPACE
words = sum([len(line.split()) for line in text])
# CONVERT LIST TO STRING LINE
text = "".join(text)
# BREAK STRING LINE TO LIST FOR STORE EACH CHARACTOR
# AND REPLACE WHITE SPACE " " WITH ""
text = [char.replace(" ", "") for char in text]
# COUNT NUMBER OF CHARACTS
characts = len(text)

# PRINT OUT THE RESULT
print("Characters:", characts)
print("Words:", words)
print("Lines:", lines)
