# OPEN FILE NAME FROM USER INPUT
sourceFile = open(input("Source file name: "))
# CREATE TARGET FILE WITH WRITTING MODE; FILE NAME FROM USER INPUT
targetFile = open(input("Target file name: "), mode = "w")

# ACCUMURATE NUMBER OF EMPTY LINE
emptyLine = 0

# AIM LOOP FOR READING SOURCE FILE BY EACH LINE
for line in sourceFile:
    # THE NUMBER OF LINE LENGTH MORE THAN 1 CHAR != EMPTY LINE
    # PROCESS TO WRITE TARGET FILE BY COPYING FROM CURRENT LINE OF SOURCE FILE
    # IF EMPTY LINE INCEASE THE ACCUMURATOR
    if len(line) > 1:
        targetFile.write(line)
    else:
        emptyLine += 1

# TERMINATE STREAMMING ACCESS FROM BOLT FILES
sourceFile.close()
targetFile.close()

# PRINT OUT ACCUMURATOR
print("Lines removed:", emptyLine)
