# OPEN FILE NAME FROM USER INPUT
sourceFile = open(input("File name: "))
# READING FILE FOR EACH LINE, COVERT A LINE TO STRING LIST
scores = [line.split() for line in sourceFile]
# TERMINATE STREAMMING FILE
sourceFile.close()

# ACUMURATE LINE NUMBER
lineNum = 0

# CONVERT str LIST TO int LIST
for line in scores:
    lineNum += 1
    # EXTRACT NUMBER FROM LINE AND CONVERT TO INTEGER
    numbers = [int(number) for number in line]
    # CALCULATE THE AVERAGE OF A LINE
    average = sum(numbers) / len(numbers)
    # PRINT OUT THE RESULT AVERAGE OF A LINE
    print("The average of line", lineNum, "is", average)
