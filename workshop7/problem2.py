# OPEN FILE NAME FROM USER INPUT
sourceFile = open(input("File name: "))

# CACHE THE ENTIRE DATA IN OPENED FILE
# ACUMURATE IN data LIST
data = [line for line in sourceFile]

# GRAB ONLY FIRST 2 AND LIST 2 ITEMS
data = data[:2] + data[-2:]

# TERMINATE STREAMMING FILE
sourceFile.close()

# EXTRACT LIST AND PRINT OUT THE RESULT
print("Output:\n", *data, sep="")
